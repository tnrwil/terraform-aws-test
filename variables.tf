variable "region" {
	description = "Default region for ecs"
	default = "us-east-1"
}

variable "ami" {
	description = "Default ami based on region"
	type = "map"
	default = {
		us-east-1 = "ami-6871a115"
#		us-east-2 = "ami-09479453c5cde9639"
	}
}


variable "instance_type" {
	description = "the instance type"
	default = "t2.small"
}

#variable "aws_access_key_id" {}
#variable "aws_secret_access_key" {}