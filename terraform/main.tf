provider "aws" {
  region = "${var.region}"
  access_key = "${{aws_access_key_id}}"
  secret_key = "${{var.aws_secret_access_key}}"
}

resource "aws_subnet" "mytest-public" {
  vpc_id     = "${aws_vpc.cie-mytest.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags {
    Name = "mytest-public"
  }
}

resource "aws_subnet" "mytest-private" {
  vpc_id     = "${aws_vpc.cie-mytest.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "us-east-1a"

  tags {
    Name = "mytest-private"
  }
}



resource "aws_vpc" "cie-mytest" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  instance_tenancy = "default"
  tags {
    Name = "cie-mytest"
  }
}


resource "aws_security_group" "cie-mytest-sg" {
    name = "vpc_rules"
    description = "Allow traffic to pass from the private subnet to the internet"
    vpc_id = "${aws_vpc.cie-mytest.id}"    
    
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"] 
    }   
    ingress {
        from_port = 3000 
        to_port = 3000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"] 
    }   
    ingress {
        from_port = 443
        to_port = 443 
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 8200 
        to_port = 8200
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
   
    ingress {
        from_port = 8201 
        to_port = 8201
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
    ingress {
        from_port = 8300 
        to_port = 8300
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
    ingress {
        from_port = 8301 
        to_port = 8301
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
    ingress {
        from_port = 8500 
        to_port = 8500
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
    ingress {
        from_port = 8600 
        to_port = 8600
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
   ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
   }
   egress {
        from_port = 0 
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
   }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.cie-mytest.id}"
}



resource "aws_network_acl" "igw_acl" {
  vpc_id = "${aws_vpc.cie-mytest.id}"

  egress {
    protocol   = "icmp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = -1 
    to_port    = -1
  }

  ingress {
    protocol   = "icmp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = -1
    to_port    = -1
  }

  tags {
    Name = "igw_acl"
  }
}

resource "aws_route_table" "cie-mytest-route" {
  vpc_id = "${aws_vpc.cie-mytest.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id  = "${aws_internet_gateway.gw.id}"
  }

  tags {
    Name = "cie-mytest-routes"
  }
}


resource "aws_route_table_association" "mytest-route-table-association" {
  subnet_id      = "${aws_subnet.mytest-public.id}"
  route_table_id = "${aws_route_table.cie-mytest-route.id}"
}

output "public_ips" {
  value = ["${aws_instance.ec2-instance.*.public_ip}"]
}

resource "aws_instance" "ec2-instance" {
  ami = "${lookup(var.ami, var.region)}"
  count = 3 
  instance_type = "${var.instance_type}"
  subnet_id = "${aws_subnet.mytest-public.id}"
  associate_public_ip_address = true
  vpc_security_group_ids = ["${aws_security_group.cie-mytest-sg.id}"]
  key_name = "tam-cie"
  provisioner "local-exec" {
     command = "echo '[consul-servers]' > hosts.txt"
  }
}
